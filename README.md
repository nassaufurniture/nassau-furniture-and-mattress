Here at Nassau Furniture, we can meet and beat the big box store prices, while still giving you the personalized shopping experience that only a local business can provide. Visit our showroom today for high quality, affordable furniture in Long Island and the surrounding NYC areas.

Address: 105 Fulton Ave, Hempstead, NY 11550, USA

Phone: 516-483-0647

Website: http://nassaufurniture.com/
